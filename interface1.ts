interface Rectangle {
    width: number;
    height: number;
    
  }

  interface ColoredRectangle extends Rectangle {
    color: string
  }
  
  const rectangle: Rectangle = {
    height: 20,
    width: 10
  }
  console.log(rectangle)

  const coloredRectangle: ColoredRectangle = {
    height: 20,
    width: 10,
    color: "red"
  }
  console.log(coloredRectangle)